export interface IProduct {
  name: String,
  image: String,
  prict: Number,
  desc: String,
  shortDesc: String
}